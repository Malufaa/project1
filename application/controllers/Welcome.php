<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
public function __construct()
	{
		 parent::__construct();
		 $this->load->model('welcome_model');
		 $this->load->helper(array('form', 'url'));
		
	 }
	public function index()
	{
		$this->load->view('login');
	   
	}
	
    public function login()
    {
        $username=$this->input->post('username');
      $password=$this->input->post('password');    
          $result=$this->welcome_model->login($username,$password);
         
        
          if($result){  
		   $res = $result->result();
		   
		  
			$usrgrp=$res[0]->user_group;
           if ($usrgrp==1)
                 $this->sysadmin();
            else if ($usrgrp==2)
                 $this->projcreator();
            else if ($usrgrp==3)
                  $this->projmanager();
            else if ($usrgrp==4)
                    $this->projleader();
            else
                 $this->projmemb();
                       
       
        }
         else    
        {
            
            $data['message']="Incorrect Email or password";
            
        }
    }
	
	public function sysadmin()
	{	redirect('admin');
	
	}
	
	
	public function projcreator()
	{	echo $data['message']="proj creator";
		
		
	}
	
	public function projleader()
	{	echo $data['message']="proj leader";
		
		
	}
	
	public function projmanager()
	{	echo $data['message']="proj manager";
		
		
	}
	
	public function projmemb()
	{	echo $data['message']="proj memb";
		
		
	}
	
	}

