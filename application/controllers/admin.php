<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		 parent::__construct();
		 $this->load->model('admin_model');
		 $this->load->helper(array('form', 'url'));
		
	 }


	public function index()
	{ 
			// Data to pass to the view
			$data = array();
			
			// will be http://localhost/project1
			$data['base_url'] = base_url();
			
	    // Admin Template added
		  // added by anurup 
		  $this->load->view('header.php',$data);
			// $this->load->view('admi.php');
			$this->load->view('index.php',$data);
			$this->load->view('footer.php',$data);
	}
	public function create()
	{
		$this->load->view('form.html');
	}
	
	public function usrcreate()
	{
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$user_group=$this->input->post('user_group');
		
		$signup=array(
			 'username'			=>		$username,
			 'password'			=> 		$password,
			 'user_group'		=>		$user_group
			 );
			 
		if($this->admin_model->createusr($signup))
		{
		 echo	$data['signupmessage']="User Created";
		}
		else
		{
			echo $data['signupmessage']="Your registration was unsuccessful.";
		
		}
		
	}
	
}


